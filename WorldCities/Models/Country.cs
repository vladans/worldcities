﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WorldCities.Models
{
    //Id, Ime, Internacionalni kod
    public class Country
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [StringLength(3, MinimumLength = 3, ErrorMessage = "field must be 3 characters long")]
        public string InternationalCode { get; set; }

    }
}