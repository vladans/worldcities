﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WorldCities.Models
{
    public class WorldCitiesContext : DbContext
    {
        public WorldCitiesContext() :base("name=WorldCitiesContext")
        {

        }

        public DbSet<City> Cities { get; set; }
        public DbSet<Country> Countries { get; set; }

    }
}