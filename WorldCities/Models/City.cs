﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WorldCities.Models
{
    //Id, Ime, Poštanski kod, Broj stanovnika
    public class City
    {
        public int Id { get; set; }

        public string Name { get; set; }

        [Required]
        [Range(10000, 99999, ErrorMessage = "Value must be 5 characters long.")]
        public int PostalCode { get; set; }

        [Required]
        [Range(0, int.MaxValue, ErrorMessage = "Value must be positive number.")]
        public int Population { get; set; }

        public Country Country { get; set; }
        public int CountryId { get; set; }

    }
}