﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using WorldCities.Interfaces;
using WorldCities.Models;

namespace WorldCities.Repository
{
    public class CityRepository : IDisposable, ICityRepository
    {
        private WorldCitiesContext db = new WorldCitiesContext();

        public bool Add(City city)
        {
            db.Cities.Add(city);
            int res = db.SaveChanges();
            return res > 0 ? true : false;
        }

        public bool Delete(City city)
        {
            db.Cities.Remove(city);
            int res = db.SaveChanges();
            return res > 0 ? true : false;
        }

        public IEnumerable<City> GetAll()
        {
            return db.Cities.Include(x => x.Country);
        }

        public City GetById(int id)
        {
            return db.Cities.FirstOrDefault(x => x.Id == id);
        }

        public bool Update(City city)
        {
            db.Entry(city).State = EntityState.Modified;
            try
            {
                int res = db.SaveChanges();
                return res > 0 ? true : false;
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}