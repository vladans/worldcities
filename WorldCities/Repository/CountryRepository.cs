﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using WorldCities.Interfaces;
using WorldCities.Models;

namespace WorldCities.Repository
{
    public class CountryRepository : IDisposable, ICountryRepository
    {
        private WorldCitiesContext db = new WorldCitiesContext();

        public bool Add(Country country)
        {
            db.Countries.Add(country);
            int res = db.SaveChanges();
            return res > 0 ? true : false;
        }

        public bool Delete(Country country)
        {
            db.Countries.Remove(country);
            int res = db.SaveChanges();
            return res > 0 ? true : false;
        }

        public IEnumerable<Country> GetAll()
        {
            return db.Countries;
        }

        public Country GetById(int id)
        {
            return db.Countries.FirstOrDefault(x => x.Id == id);
        }

        public bool Update(Country country)
        {
            db.Entry(country).State = EntityState.Modified;

            try
            {
                int res = db.SaveChanges();
                return res > 0 ? true : false;
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}