﻿FTN Informatika
Kurs iz .NET Web programiranja
=================================


1. Sadržaj vežbe
-------------------
- AutoMapper biblioteka za objekat-objekat mapiranje.
- Dependency Injection koncept. Primer sa repository šablonom.
- ASP.NET Web API jedinično testiranje kontrolera.


3. Literatura
-------------------
- materijali/Modul03-Termin08.pdf


4. Primeri
-------------------
- BookService/
- ProductService/


5. Zadatak na času
-------------------
1) Napraviti REST API za rad sa Gradovima (Id, Ime, Poštanski kod, Broj stanovnika) i Državama (Id, Ime, Internacionalni kod).
   Svaki grad pripada tačno jednoj državi.
   Prikaz svih gradova je sortiran po poštanskom kodu rastuće.
   Omogućiti pretragu gradova sa brojem stanovnika između 2 uneta broja.
   Omogućiti na endpoint-u GET /api/statistics prikaz država i njihovog broja stanovnika, sortirano po broju stanovnika opadajuće.
   Jedinično testirati kontrolere.
   Kreirati JQuery klijenta koji će konzumirati implementirani API. Prilikom kreiranja Grada, Država se bira iz padajućeg menija sa svim državama (drop-down). 
   Dodatno: Omogućiti učitavanje sadržaja u tabelu bez klika na dugme.

   
6. Domaći zadatak
-------------------
1) Neurađeni zadaci sa časa.

context.Countries.AddOrUpdate(x => x.Id,
                new Country() { Id = 1, Name = "Serbia", InternationalCode = "SRB"},
                new Country() { Id = 2, Name = "Monte Negro", InternationalCode = "MNG" },
                new Country() { Id = 3, Name = "Bosnia", InternationalCode = "BIH" },
                new Country() { Id = 4, Name = "Croatia", InternationalCode = "CRO" }
            );

            context.Cities.AddOrUpdate(x => x.Id,
                new City() { Id = 1, PostalCode = 21000, InternationalCode = "SRB" },
                new City() { Name = "Monte Negro", InternationalCode = "MNG" },
                new City() { Name = "Bosnia", InternationalCode = "BIH" },
                new City() { Name = "Croatia", InternationalCode = "CRO" }
            );
