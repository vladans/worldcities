﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorldCities.Models;

namespace WorldCities.Interfaces
{
    public interface ICityRepository
    {
        IEnumerable<City> GetAll();
        City GetById(int id);
        bool Add(City city);
        bool Update(City city);
        bool Delete(City city);
    }
}
