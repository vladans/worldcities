﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorldCities.Models;

namespace WorldCities.Interfaces
{
    public interface ICountryRepository
    {
        IEnumerable<Country> GetAll();
        Country GetById(int id);
        bool Add(Country country);
        bool Update(Country country);
        bool Delete(Country country);
    }
}
