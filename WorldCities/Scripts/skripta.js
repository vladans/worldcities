$(document).ready(function(){

	// podaci od interesa
	var host = "http://localhost:";
    var port = "63128/";
    var countriesEndpoint = "api/countries/";
    var citiesEndpoint = "api/cities/";
	var formActionCountry = "Create";
	var formActionCity = "Create";
	var editingIdCountry;
	var editingIdCitye;
	var editingIdCityCountry;

	// pripremanje dogadjaja za brisanje
	$("body").on("click", "#btnDeleteCountry", deleteCountry);
    $("body").on("click", "#btnDeleteCity", deleteCity);

	// priprema dogadjaja za izmenu
	$("body").on("click", "#btnEditCountry", editCountry);
	$("body").on("click", "#btnEditCity", editCity);

	// prikaz tabele
	$("#btnCountries").click(function(){
		// sakrivanje druge tabele
		var $tableCities = $("#dataCity");
		$tableCities.empty();
		// sakrivanje druge forme
		$("#formDivCity").css("display","none");
		// ucitavanje podataka
		var requestUrl = host + port + countriesEndpoint;
		console.log("URL zahteva: " + requestUrl);
		$.getJSON(requestUrl, setCountries);
	});

	// prikaz tabele
	$("#btnCities").click(function(){
		// sakrivanje druge tabele
		var $tableCountries = $("#dataCountry");
        $tableCountries.empty();
		// sakrivanje druge forme
        $("#formDivCountry").css("display","none");
		// ucitavanje podataka
		var requestUrl = host + port + citiesEndpoint;
		console.log("URL zahteva: " + requestUrl);
        $.getJSON(requestUrl, setCities);
        setSelectListCountries();
    });

    // metoda za popunjavane select liste drzava
    function setSelectListCountries() {
        // popunjavamo selectList
        var selectList = document.getElementById("cityCountry");
        $.ajax({
            url: host + port + countriesEndpoint,
            type: "GET",
        })
            .done(function (data, status) {
                if ($('#cityCountry option').length == 0) {
                    for (var i = 0; i < data.length; i++) {
                        var option = document.createElement('option');
                        option.value = data[i].Id;
                        if (data[i].Id == 1) {
                            option.selected = true;
                        }
                        option.text = data[i].Name;
                        selectList.add(option, 0);
                    }
                    //$('#cityCountry').hide();
                }
            })
            .fail(function (data, status) {
                alert("Desila se greska prilikom popunjavanja selectList drzava!");
            });
    }

	// metoda za postavljanje podataka u tabelu
	function setCountries(data, status){
		console.log("Status: " + status);

		var $container = $("#dataCountry");
		$container.empty(); 
		
		if (status == "success")
		{
			console.log(data);
			// ispis naslova
			var div = $("<div></div>");
			var h1 = $("<h1>Drzave</h1>");
			div.append(h1);
			// ispis tabele
			var table = $("<table></table>");
			var header = $("<tr><td>Id</td><td>Name</td><td>International code</td><td>Delete</td><td>Edit</td></tr>");
			table.append(header);
			for (i=0; i<data.length; i++)
			{
				// prikazujemo novi red u tabeli
				var row = "<tr>";
				// prikaz podataka
                var displayData = "<td>" + data[i].Id + "</td><td>" + data[i].Name + "</td><td>" + data[i].InternationalCode+"</td>";
				// prikaz dugmadi za izmenu i brisanje
				var stringId = data[i].Id.toString();
				var displayDelete = "<td><button id=btnDeleteCountry name=" + stringId + ">Delete</button></td>";
                var displayEdit = "<td><button id=btnEditCountry name=" + stringId + ">Edit</button></td>";
				row += displayData + displayDelete + displayEdit + "</tr>";  
				table.append(row);
				newId = data[i].Id;
			}
			
			div.append(table);

			// prikaz forme
			$("#formDivCountry").css("display","block");

			// ispis novog sadrzaja
			$container.append(div);
		}
		else 
		{
			var div = $("<div></div>");
			var h1 = $("<h1>Greška prilikom preuzimanja drzava!</h1>");
			div.append(h1);
			$container.append(div);
		}
	};

	// metoda za postavljanje podataka u tabelu
	function setCities(data, status){
		console.log("Status: " + status);

		var $container = $("#dataCity");
        $container.empty();
        $("#cityName").value = "";
        $("#cityCode").value = "";
        $("#cityPopulation").value = "";
        $('#cityCountry').val(0);

		
		if (status == "success")
		{
			console.log(data);
			// ispis naslova
			var div = $("<div></div>");
			var h1 = $("<h1>Gradovi</h1>");
			div.append(h1);
			// ispis tabele
			var table = $("<table></table>");
			var header = $(`
				<tr>
					<td>Id</td>
					<td>Name</td>
					<td>Postal code</td>
					<td>Populacion</td>
					<td>Country</td>
					<td>Delete</td>
					<td>Edit</td>
				</tr>`);
			table.append(header);
			for (i=0; i<data.length; i++)
			{
				// prikazujemo novi red u tabeli
				var row = "<tr>";
				// prikaz podataka
				var displayData = 	"<td>"+data[i].Id+"</td>"
									+"<td>"+data[i].Name+"</td>"
                                    +"<td>"+data[i].PostalCode+"</td>"
                                    +"<td>"+data[i].Population+"</td>"
                                    +"<td>"+data[i].Country.Name+"</td>";
				// prikaz dugmadi za izmenu i brisanje
				var stringId = data[i].Id.toString();
				var displayDelete = "<td><button id=btnDeleteCity name=" + stringId + ">Delete</button></td>";
				var displayEdit = "<td><button id=btnEditCity name=" + stringId + ">Edit</button></td>";
				row += displayData + displayDelete + displayEdit + "</tr>";  
				table.append(row);
				newId = data[i].Id;
			}
			
			div.append(table);

			// prikaz forme
			$("#formDivCity").css("display","block");

			// ispis novog sadrzaja
			$container.append(div);
		}
		else 
		{
			var div = $("<div></div>");
			var h1 = $("<h1>Greška prilikom preuzimanja gradova!</h1>");
			div.append(h1);
			$container.append(div);
		}
	};

	// dodavanje nove drzave
	$("#countryForm").submit(function(e){
		// sprecavanje default akcije forme
		e.preventDefault();

        var countryName = $("#countryName").val();
        var countryCode = $("#countryCode").val();
		var httpAction;
		var sendData;
		var url;

		// u zavisnosti od akcije pripremam objekat
		if (formActionCountry === "Create") {
			httpAction = "POST";
			url = host + port + countriesEndpoint;
			sendData = {
                "Name": countryName,
                "InternationalCode": countryCode
			};
		}
		else {
			httpAction = "PUT";
            url = host + port + countriesEndpoint + editingIdCountry.toString();
			sendData = {
                "Id": editingIdCountry,
                "Name": countryName,
                "InternationalCode": countryCode
			};
		}
		
		console.log("Objekat za slanje");
		console.log(sendData);

		$.ajax({
			url: url,
			type: httpAction,
			data: sendData
		})
		.done(function(data, status){
			formActionCountry = "Create";
            refreshTableCountry();
		})
		.fail(function(data, status){
			alert("Desila se greska!");
		})
	});

	// dodavanje novog grada
	$("#cityForm").submit(function(e){
		// sprecavanje default akcije forme
		e.preventDefault();

        var cityName = $("#cityName").val();
        var cityCode = $("#cityCode").val();
        var cityPopulation = $("#cityPopulation").val();
        var cityCountry = $("#cityCountry").val();
		var httpAction;
		var sendData;
		var url;

		// u zavisnosti od akcije pripremam objekat
		if (formActionCity === "Create") {
			httpAction = "POST";
			url = host + port + citiesEndpoint;
			sendData = {
                "Name": cityName,
                "PostalCode": cityCode,
                "Population": cityPopulation,
                "CountryId": cityCountry
			};
		}
		else {
			httpAction = "PUT";
			url = host + port + citiesEndpoint + editingIdCity.toString();
			sendData = {
                "Id": editingIdCity,
                "Name": cityName,
                "PostalCode": cityCode,
                "Population": cityPopulation,
                "CountryId": cityCountry
			};
		}
		
		console.log("Objekat za slanje");
		console.log(sendData);

		$.ajax({
			url: url,
			type: httpAction,
			data: sendData
		})
		.done(function(data, status){
			formActionCity = "Create";
			refreshTableCity();
		})
		.fail(function(data, status){
			alert("Desila se greska!");
		})
	});

	// brisanje drzave
	function deleteCountry() {
		// izvlacimo {id}
		var deleteID = this.name;
		// saljemo zahtev 
		$.ajax({
			url: host + port + countriesEndpoint + deleteID.toString(),
			type: "DELETE",
		})
		.done(function(data, status){
			refreshTableCountry();
		})
		.fail(function(data, status){
			alert("Desila se greska!");
		});
	};

	// brisanje grada
	function deleteCity() {
		// izvlacimo {id}
		var deleteID = this.name;
		// saljemo zahtev 
		$.ajax({
			url: host + port + citiesEndpoint + deleteID.toString(),
			type: "DELETE",
		})
		.done(function(data, status){
			refreshTableCity();
		})
		.fail(function(data, status){
			alert("Desila se greska!");
		});
	};

	// izmena drzave
	function editCountry(){
		// izvlacimo id
		var editId = this.name;
		// saljemo zahtev da dobavimo tu drzavu
		$.ajax({
			url: host + port + countriesEndpoint + editId.toString(),
			type: "GET",
		})
		.done(function(data, status){
			$("#countryName").val(data.Name);
            $("#countryCode").val(data.InternationalCode);
			editingIdCountry = data.Id;
			formActionCountry = "Update";
		})
		.fail(function(data, status){
			formActionCountry = "Create";
			alert("Desila se greska!");
		});
	};

	// izmena grada
	function editCity(){
		//// popunjavamo selectList
		//var selectList = document.getElementById("cityCountry");
		//$.ajax({
		//	url: host + port + countriesEndpoint,
		//	type: "GET",
		//})
		//.done(function(data, status){
		//	for(var i=0; i < data.length; i++) {
		//        var option = document.createElement('option');
		//        option.value = data[i].Id;
        //        option.text = data[i].Name;
		//        selectList.add(option, 0);
		//    }
		//})
		//.fail(function(data, status){
		//	alert("Desila se greska prilikom popunjavanja selectList drzava!");
		//});

		// izvlacimo id
		var editId = this.name;
		// saljemo zahtev da dobavimo taj grad
		$.ajax({
			url: host + port + citiesEndpoint + editId.toString(),
			type: "GET",
		})
		.done(function(data, status){
			$("#cityName").val(data.Name);
            $("#cityCode").val(data.PostalCode);
            $("#cityPopulation").val(data.Population);
            $('#cityCountry').val(data.CountryId);
			//document.getElementById('cityCountry').value=data.CountryId;
			//selectList.selected = true;
			editingIdCity = data.Id;
            editingIdCityCountry = data.CountryId;
			formActionCity = "Update";
		})
		.fail(function(data, status){
			formActionCity = "Create";
			alert("Desila se greska!");
		});
	};

	// osvezi prikaz tabele
	function refreshTableCountry() {
		// cistim formu
		$("#countryName").val('');
        $("#countryCode").val('');
		// osvezavam
		$("#btnCountries").trigger("click");
	};

	// osvezi prikaz tabele
	function refreshTableCity() {
		// cistim formu
		$("#cityName").val('');
		$("#cityCode").val('');
        $("#cityPopulation").val('');
		// osvezavam
		$("#btnCities").trigger("click");
	};
});
