namespace WorldCities.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using WorldCities.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<WorldCities.Models.WorldCitiesContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(WorldCities.Models.WorldCitiesContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            context.Countries.AddOrUpdate(x => x.Id,
                new Country() { Id = 1, Name = "Serbia", InternationalCode = "SRB" },
                new Country() { Id = 2, Name = "Monte Negro", InternationalCode = "MNG" },
                new Country() { Id = 3, Name = "Bosnia", InternationalCode = "BIH" },
                new Country() { Id = 4, Name = "Croatia", InternationalCode = "CRO" }
            );

            context.Cities.AddOrUpdate(x => x.Id,
                new City() { Id = 1, Name = "Novi Sad", PostalCode = 21000, Population = 7000000, CountryId = 1 },
                new City() { Id = 2, Name = "Sombor", PostalCode = 25000, Population = 50000, CountryId = 1 },
                new City() { Id = 3, Name = "Podgorica", PostalCode = 81000, Population = 186000, CountryId = 2 },
                new City() { Id = 4, Name = "Budva", PostalCode = 85300, Population = 57000, CountryId = 2 },
                new City() { Id = 5, Name = "Sarajevo", PostalCode = 71000, Population = 276000, CountryId = 3 },
                new City() { Id = 6, Name = "Banja Luka", PostalCode = 78000, Population = 200000, CountryId = 3 },
                new City() { Id = 7, Name = "Zagreb", PostalCode = 10000, Population = 801000, CountryId = 4 },
                new City() { Id = 8, Name = "Osijek", PostalCode = 31000, Population = 106000, CountryId = 4 }
            );
        }
    }
}
