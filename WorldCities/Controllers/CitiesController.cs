﻿using System.Collections.Generic;
using System.Web.Http;
using WorldCities.Interfaces;
using WorldCities.Models;

namespace WorldCities.Controllers
{
    public class CitiesController : ApiController
    {
        ICityRepository _repository { get; set; }
        public CitiesController(ICityRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<City> Get()
        {
            return _repository.GetAll();
        }

        public IHttpActionResult Get(int id)
        {
            var city = _repository.GetById(id);
            if (city == null)
            {
                return NotFound();
            }
            return Ok(city);
        }

        public IHttpActionResult Post(City city)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _repository.Add(city);
            return CreatedAtRoute("DefaultApi", new { id = city.Id }, city);
        }

        public IHttpActionResult Put(int id, City city)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != city.Id)
            {
                return BadRequest();
            }

            try
            {
                _repository.Update(city);
            }
            catch
            {
                return BadRequest();
            }

            return Ok(city);
        }

        public IHttpActionResult Delete(int id)
        {
            var city = _repository.GetById(id);
            if (city == null)
            {
                return NotFound();
            }

            _repository.Delete(city);
            return Ok();
        }
    }
}
